# Mina-Sanmple
MinaProtocolを使ったZKApp開発用のリポジトリです。

## zkAppsとは何？
zkApps（ゼロ知識アプリ）は、ゼロ知識証明、特にzk-SNARKを利用したMinaProtocolのスマートコントラクトです。

zkAppsはオフチェーン実行とオフチェーンステートモデルを使用します。これにより、プライベートな計算と、プライベートにもパブリックにもなり得る状態を実現します。

zkAppsは、チェーン上で計算を実行し、可変ガス料金ベースのモデルを使用する他のブロックチェーンとは対照的に、この計算の検証のために、結果のゼロ知識証明をチェーンに送信するためのフラット料金のみを負担しながら、チェーン外で任意に複雑な計算を実行できます。

## ゼロ知識ベースのスマートコントラクト
zkAppはゼロ知識証明（zk-SNARK）に基づいているため、zkAppの開発者は「回路」と呼ばれるものを書きます。回路とは、構築プロセスにおいて、証明者関数と対応する検証者関数が導き出される方法である。

プロバー関数は、スマートコントラクトのカスタムロジックを実行する関数です。

プロバー関数は、zkAppの一部としてユーザーのWebブラウザで実行されます。zkAppのUIと対話する際、ユーザーは必要なデータ（例えば、「ABCをyの価格で買う」）を入力し、プロバー関数はゼロ知識証明を生成する。

プロバー機能とは、スマートコントラクトのカスタムロジックを実行する機能です。
プライベート入力とパブリック入力の両方が、ユーザーのウェブブラウザで実行されるときに、プロバー関数に提供されなければならないデータである。

プライベート入力は、その時点以降、再び必要になることはありません。しかし、パブリック入力は、検証関数がMinaネットワーク上で実行されるときにも提供されなければならないので、非公開にしたいデータには決して使用しないでください。

検証関数は、ゼロ知識証明が証明者関数で定義されたすべての制約をうまく通過しているかどうかを検証する関数である。これは、証明者関数の複雑さに関係なく、常に迅速かつ効率的に実行される。

Minaネットワーク内では、Minaが検証者としての役割を果たし、検証者機能を実行する。

www.DeepL.com/Translator（無料版）で翻訳しました。

## CLIのインストール方法

```bash
npm install -g zkapp-cli
```

## プロジェクトの作成方法

```bash
zk project my_first_zkapp
```

諸々聞かれるので自分の好みで答えてテンプレを作成する。

```bash
✔ Create an accompanying UI project too? · next
✔ Do you want to setup your project for deployment to Github Pages? · yes
Need to install the following packages:
  create-next-app@13.4.1
Ok to proceed? (y) y
✔ Would you like to use TypeScript with this project? … No / Yes
✔ Would you like to use ESLint with this project? … No / Yes
✔ Would you like to use Tailwind CSS with this project? … No / Yes
Creating a new Next.js app in /workspaces/Mina-Sanmple/my_first_zkapp/ui.

Using npm.

Initializing project with template: default-tw 


Installing dependencies:
- react
- react-dom
- next
- typescript
- @types/react
- @types/node
- @types/react-dom
- tailwindcss
- postcss
- autoprefixer
- eslint
- eslint-config-next


added 344 packages, and audited 345 packages in 21s

127 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
Success! Created ui at /workspaces/Mina-Sanmple/my_first_zkapp/ui

A new version of `create-next-app` is available!
You can update by running: npm i -g create-next-app

Using project name my_first_zkapp for github repo name. Please change in next.config.js and pages/reactCOIServiceWorker.tsx if this is not correct or changes
✔ COI-ServiceWorker: NPM install
✔ UI: Set up project
✔ UI: NPM install
✔ Initialize Git repo
✔ Set up project
✔ NPM install
✔ NPM build contract
✔ Set project name
✔ Git init commit

Success!

Next steps:
  cd my_first_zkapp
  git remote add origin <your-repo-url>
  git push -u origin main
```

## プロジェクトの設定方法

```bash
zk config
```

実行結果

```bash
✔ Create a name (can be anything): · berkeley
✔ Set the Mina GraphQL API URL to deploy to: · https://proxy.berkeley.minaexplorer.com/graphql
✔ Set transaction fee to use when deploying (in MINA): · 0.1
✔ Choose an account to pay transaction fees: · Recover fee payer account from an existing base58 private key
✔ Create an alias for this account · devAccount
✔ Account private key (base58):
  NOTE: the private key will be stored in plain text on this computer.
  Do NOT use an account which holds a substantial amount of MINA. · 
✔ Recover fee payer keypair from  and add to /home/codespace/.cache/zkapp-cli/keys/devaccount.json
✔ Create zkApp key pair at keys/berkeley.json
✔ Add deploy alias to config.json

Success!

Next steps:
  - If this is a testnet, request tMINA at:
    https://faucet.minaprotocol.com/?address=B62qjEXDGUAA9Z5o6Qh7Qcrx6udVbY7D7C4fqsAqRSr9rE8SNLod69V&?explorer=minaexplorer
  - To deploy, run: `zk deploy berkeley`
```

### コントラクトのデプロイ

```bash
zk deploy berkeley
```

実行結果


```bash
✔ Build project
✔ Generate build.json
✔ Choose smart contract
  Only one smart contract exists in the project: Add
  Your config.json was updated to always use this
  smart contract when deploying to this deploy alias.
✔ Generate verification key (takes 10-30 sec)
✔ Build transaction
✔ Confirm to send transaction

  ┌─────────────────┬─────────────────────────────────────────────────┐
  │ Deploy Alias    │ berkeley                                        │
  ├─────────────────┼─────────────────────────────────────────────────┤
  │ Fee-Payer Alias │ undefined                                       │
  ├─────────────────┼─────────────────────────────────────────────────┤
  │ URL             │ https://proxy.berkeley.minaexplorer.com/graphql │
  ├─────────────────┼─────────────────────────────────────────────────┤
  │ Smart Contract  │ Add                                             │
  └─────────────────┴─────────────────────────────────────────────────┘
  
  Are you sure you want to send (yes/no)? · yes
✔ Send to network

Success! Deploy transaction sent.

Next step:
  Your smart contract will be live (or updated)
  as soon as the transaction is included in a block:
  https://berkeley.minaexplorer.com/transaction/5JuuQuTpFLY5xtuBjiGceZ8tLriQE8poj7QHM2NVskunVF9DwvSi
```

### 参考文献
1. [Minaprotocol getstarted](https://minaprotocol.com/get-started)
2. [Minaprotocol Quickstart](https://docs.minaprotocol.com/zkapps#quickstart)
3. [Minaprottocol faucet](https://faucet.minaprotocol.com/?address=B62qjEXDGUAA9Z5o6Qh7Qcrx6udVbY7D7C4fqsAqRSr9rE8SNLod69V&?explorer=minaexplorer)
4. [berkeley.minaexplorer.](https://berkeley.minaexplorer.com/)
5. [Tutorial- Hello world!](https://docs.minaprotocol.com/zkapps/tutorials/hello-world)