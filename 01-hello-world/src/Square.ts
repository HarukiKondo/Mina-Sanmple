import {
    Field,
    SmartContract,
    state,
    State,
    method,
} from 'snarkyjs';

/**
 * Square Contract
 */
export class Square extends SmartContract {
    @state(Field) num = State<Field>();

    /**
     * 初期化メソッド
     */
    init() {
        super.init();
        this.num.set(Field(3));
    }

    /**
     * updateメソッド
     */
    @method update(square: Field) {
        // get num 
        const currentState = this.num.get();
        // check
        this.num.assertEquals(currentState);
        // square
        square.assertEquals(currentState.mul(currentState));
        // set square
        this.num.set(square);
    }
}